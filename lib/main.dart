import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          title: const Text(
            'Marimba Músical',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 26.0,
              letterSpacing: 1.0,
              color: Colors.white,
            ),
          ),
        ),
        body: const CorpoPrincipal(),
      ),
    );
  }
}

class CorpoPrincipal extends StatefulWidget {
  const CorpoPrincipal({super.key});

  @override
  State<CorpoPrincipal> createState() => _CorpoPrincipalState();
}

class _CorpoPrincipalState extends State<CorpoPrincipal> {
  void audio({int? number}) async {
    AudioCache.instance = AudioCache(prefix: '');
    final player = AudioPlayer();
    await player.play(AssetSource('assets/audio/nota$number.wav'));
  }

  Expanded botao({Color? cor, int? valor}) {
    return Expanded(
      child: ElevatedButton(
        style: TextButton.styleFrom(
          backgroundColor: cor,
          shape: const BeveledRectangleBorder(
            borderRadius: BorderRadius.all(Radius.zero),
          ),
        ),
        onPressed: () {
          audio(number: valor);
        },
        child: const Center(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        botao(cor: Colors.greenAccent, valor: 1),
        botao(cor: Colors.yellowAccent, valor: 2),
        botao(cor: Colors.cyanAccent, valor: 3),
        botao(cor: Colors.deepOrange, valor: 4),
        botao(cor: Colors.indigoAccent, valor: 5),
        botao(cor: Colors.deepPurpleAccent, valor: 6),
        botao(cor: Colors.purpleAccent, valor: 7),
      ],
    );
  }
}
